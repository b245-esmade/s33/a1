
// 3-4

		fetch("https://jsonplaceholder.typicode.com/todos", {method: "GET"})

		.then(response => response.json())
		.then(result => {
			let title = result.map(element => element.title)
		    console.log(title);
		})



// 5-6

		fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "GET"})

		.then(response => response.json())
		.then(result => console.log(result));


//7

		fetch("https://jsonplaceholder.typicode.com/todos/", {method:"POST",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            title:"inhale exhale",
            completed:"true"
        })
		})
		.then(response => response.json())
		.then(result => console.log(result));



//8
		fetch("https://jsonplaceholder.typicode.com/todos/1", {method:"PUT",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            title:"mirror the data structure used in the PUT fetch request",
            completed:"true"
        })
		})
		.then(response => response.json())
		.then(result => console.log(result));

	

//9-10

		fetch("https://jsonplaceholder.typicode.com/todos/9",{method:"PATCH",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            title:"Javascript to-do",
            description:"eat Javascript",
            completed: "false",
            dateCompleted:"01/09/2022",
            userId:"1"
        })
		})
		.then(response => response.json())
		.then(result => console.log(result));


//11

		fetch("https://jsonplaceholder.typicode.com/todos/9",{method:"PATCH",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            completed:"true",
            dateCompleted:"01/10/2022"
        })
		})
		.then(response => response.json())
		.then(result => console.log(result));


//12
		fetch("https://jsonplaceholder.typicode.com/posts/3",{method:"PUT"})
		
		.then(response => response.json())
		.then(result => console.log(result));

